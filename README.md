glicko2-go
==========

Go implementation of Glicko2 ranking algorithm from http://www.glicko.net/glicko/glicko2.pdf
